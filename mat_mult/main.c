#include <stdio.h>
#include <stdlib.h>
#include "mat_mult.h"


//Liest A von der Standardeingabe ein. Gibt 0 zurück, falls Einlesen erfolgreich ist, sonst -1
int read_mat(int n, int m, int A[]){
  int result;
  for (int i = 0; i < n; i++) {
    printf("Bitte geben Sie die %i. Zeile der Matrix an\n", i+1 );
    for (int j = 0; j < m; j++) {
      printf("%i-ter Eintrag:\n", j+1 );
      scanf("%i", &A[i*n+j]);
    }
  }
}


void write_mat(int n, int m, int A[]){
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < m; j++) {
      printf("%i ", A[i*n+j]);
    }
    printf("\n");
  }
}


int main(void) {
  int n,m,o;
  printf("Dieses Programm multipliziert eine nxm-Matrix A mit einer mxo-Matrix B:\n");
  printf("Bitte geben Sie n an:");
  scanf("%d",&n);
  printf("Bitte geben Sie m an:");
  scanf("%d",&m);
  printf("Bitte geben Sie o an:");
  scanf("%d",&o);
  //Eingabe
  int A[n*m];
  printf("Bitte geben Sie A an:\n");
  if(read_mat(n,m,A) == -1){
    printf("Eingabe Fehlgeschlagen!\n");
    return -1;
  }
  int B[m*o];
  printf("Bitte geben Sie B an:\n");
  if(read_mat(m,o,B) == -1){
    printf("Eingabe Fehlgeschlagen!\n");
    return -1;
  }
  int C[n*o];

  printf("n:%i m:%i o:%i:\n",n,m,o);
  printf("A[1,1]:%i A[1,2]:%i A[2,1]:%i A[2,2]:%i\n",A[0],A[1],A[2],A[3]);
  printf("B[1,1]:%i B[1,2]:%i B[2,1]:%i B[2,2]:%i\n",B[0],B[1],B[2],B[3]);



  mat_mult(n,m,o,A,B,C);

  //Ausgabe
  printf("Das Produkt der beiden Faktoren ist:\n");
  write_mat(n,o,C);
  return 0;
}
